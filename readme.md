# [Task #6. Character device driver](https://gl-khpi.gitlab.io/#practice-tasks)

---

## Description

Implement a character device driver for text messaging between users.

---

## References

- [procfs demo](https://gitlab.com/gl-khpi/examples/tree/master/procfs_rw).
- [sysfs demo](https://gitlab.com/gl-khpi/examples/tree/master/sysfs).
- [LCD demo](https://gitlab.com/gl-khpi/examples/-/tree/master/lcd_3.2_rpi).

---

## Guidance

> Use [Git](https://gl-khpi.gitlab.io/#commit-requirements) and the following names: `task06` - for your project's home directory; `partXX` - for the directory of each subtask (`XX` - subtask number); `src` - for the source code directory.

:one: **Subtask #1**. Implement a character device driver with the following requirements:

- the default buffer size is 1 kB; :ballot_box_with_check:
- set the buffer size by the module parameter; :ballot_box_with_check:
- should be available for all users; :ballot_box_with_check:
- works only ASCII strings. :ballot_box_with_check:

:two: **Subtask #2**. Add the interfaces (sysfs, procfs) into the driver:

- to select users; :ballot_box_with_check:
- to clear the buffer; :ballot_box_with_check:
- to get the buffer status (size, number of messages, amount of used space). :ballot_box_with_check:

:heart: Implement bash script for the driver testing. :ballot_box_with_check:

:yellow_heart: Provide a test scenario. :ballot_box_with_check:

---

## Extra

- Show messenger buffer status using *3.2inch RPi LCD V4* from *GL Raspberry Kit*.
- Use the interfaces (sysfs, procfs) to change the font, text and background color of the LCD.
---



# Task #6 result:
## How to use
After insert module in linux system you can see some files in `/sys/class/Messenger` directory\
and char device file in `/dev/`

<pre><font color="#FF0000"><b>root</b></font>@Pu4taz <b>/sys/class/Messenger </b># ls
buffer_size  clear_buffer  get_buffer_status  recipient  user  <font color="#00FFFF"><b>user_manager</b></font>
<font color="#FF0000"><b>root</b></font>@Pu4taz <b>/sys/class/Messenger </b># 
</pre>

File `user_manager` has another color because it's a char device;\
this file will change name and minor when you change user.

For change user you must run following command:
<pre><font color="#FF0000"><b>root</b></font>@Pu4taz <b>/sys/class/Messenger </b># echo "user_name" > user </pre>
---

If you want to create a new user use following syntax:
<pre><font color="#FF0000"><b>root</b></font>@Pu4taz <b>/sys/class/Messenger </b># echo "new:user_name" > user </pre>
---

Recipient for default is everyone, for change recipient use:
<pre><font color="#FF0000"><b>root</b></font>@Pu4taz <b>/sys/class/Messenger </b># echo "recipient_name" > recipient </pre>
- For change recipient, his must be created
---

You can see who recipient is now. Use next commant:
<pre><font color="#FF0000"><b>root</b></font>@Pu4taz <b>/sys/class/Messenger </b># cat recipient </pre>
---

You can do the same for a user:
<pre><font color="#FF0000"><b>root</b></font>@Pu4taz <b>/sys/class/Messenger </b># cat user </pre>
---

For clean buffer write any text in `clear_buffer` file:
<pre><font color="#FF0000"><b>root</b></font>@Pu4taz <b>/sys/class/Messenger </b># echo "some_text" > clear_buffer </pre>
---

For see current buffer size use:
<pre><font color="#FF0000"><b>root</b></font>@Pu4taz <b>/sys/class/Messenger </b># cat buffer_size </pre>
---

For change buffer size use:
<pre><font color="#FF0000"><b>root</b></font>@Pu4taz <b>/sys/class/Messenger </b># echo "new_size" > buffer_size </pre>
- Buffer size must be greater than 256 bytes, and this must be just numbers otherwise buffer size not be changed
---

And for get status of buffer use:
<pre><font color="#FF0000"><b>root</b></font>@Pu4taz <b>/sys/class/Messenger </b># cat get_buffer_size</pre>
---

### Send message
<pre><font color="#FF0000"><b>root</b></font>@Pu4taz <b>/home/tush17/lkglbasecamp/task06/part02 </b><font color="#CD00CD">(</font>git<font color="#CD00CD">)</font><font color="#CDCD00">-</font><font color="#CD00CD">[</font><font color="#00CD00">master</font><font color="#CD00CD">]</font> # cd /sys/class/Messenger 
<font color="#FF0000"><b>root</b></font>@Pu4taz <b>/sys/class/Messenger </b># ls
buffer_size  clear_buffer  get_buffer_status  recipient  user  <font color="#00FFFF"><b>user_manager</b></font>
<font color="#FF0000"><b>root</b></font>@Pu4taz <b>/sys/class/Messenger </b># ls /dev/user_manager 
<span style="background-color:#000000"><font color="#FFFF00"><b>/dev/user_manager</b></font></span>
<font color="#FF0000"><b>root</b></font>@Pu4taz <b>/sys/class/Messenger </b># 
</pre>

First user is `user_manager`, recommend to create new user\
and recipient, after change recipietn for current user

Send the message after before setting is so easy.\
Just use next command:
<pre><font color="#FF0000"><b>root</b></font>@Pu4taz <b>/sys/class/Messenger </b># echo "You messages" > /dev/[user_name]</pre>

## Testing
- Run the `make test` (test will be running in virtual machine);
- Or you can run `make all && ./test` for run in you PC;
- This script shows first the messages for the last recipients and then the entire contents of the buffer;

## Test scenario output:
### For user `Taras`
```
###################### START ##########################

=================================================================
==== TARAS START SHOWS INFO ====
#### BUFFER SIZE FOR USER 'Taras' ####
Size is: 1024

#### BUFFER STATUS BEFORE RESIZE ####

=====   INFO BUFFER STATUS   =====

Size of buffer: 1024
Number of messages: 14
Amount of used space: 641

===== END INFO BUFFER STATUS =====

#### RESIZE BUFFER TO 600  BYTES ####

#### BUFFER STATUS AFTER RESIZE  ####

=====   INFO BUFFER STATUS   =====

Size of buffer: 600
Number of messages: 13
Amount of used space: 564

===== END INFO BUFFER STATUS =====


####    TARAS BUFFER CONTAINE    ####

You->everyone: Hello everyone!
You->everyone: How are you?
Oleg->You: Hi, Taras.
Oleg->You: I am so sad because are creator Vlad can't come up with a good dialogue
You->Oleg: Ohh, it's bull sh*t, we wish good luck to the Vlad :)


#### TARAS FULL BUFFER CONTAINE  ####

You->everyone: Hello everyone!
You->everyone: How are you?
Vlad->You: Hi, Taras.
Vlad->You: I'm in depression!
You->Vlad: Why, what is happened!
Vlad->You: Because my favorite jym is closed forever)))
You->Vlad: Oh man, Can I help you?
Vlad->You: Maybe you will come in my house and drink some beer) and watching football?
You->Vlad: Nice idea, I gonna come to you!
Vlad->You: I will wait you.
Oleg->You: Hi, Taras.
Oleg->You: I am so sad because are creator Vlad can't come up with a good dialogue
You->Oleg: Ohh, it's bull sh*t, we wish good luck to the Vlad :)


####         CLEAR BUFFER        ####


## TARAS BUFFER STATUS AFTER CLEAR ##

=====   INFO BUFFER STATUS   =====

Size of buffer: 600
Number of messages: 0
Amount of used space: 0

===== END INFO BUFFER STATUS =====

==== TARAS END SHOWS INFO ====
=================================================================
```

---

### For user `Vlad`
---

```
=================================================================
==== VLAD START SHOWS INFO ====
#### BUFFER SIZE FOR USER 'Vlad' ####

Size is: 1024

#### BUFFER STATUS BEFORE RESIZE ####

=====   INFO BUFFER STATUS   =====

Size of buffer: 1024
Number of messages: 11
Amount of used space: 473

===== END INFO BUFFER STATUS =====

#### RESIZE BUFFER TO 2048 BYTES ####

#### BUFFER STATUS AFTER RESIZE  ####

=====   INFO BUFFER STATUS   =====

Size of buffer: 2048
Number of messages: 11
Amount of used space: 473

===== END INFO BUFFER STATUS =====


####    VLAD BUFFER CONTAINE     ####

Taras->You: Hello everyone!
Taras->You: How are you?
You->Taras: Hi, Taras.
You->Taras: I'm in depression!
Taras->You: Why, what is happened!
You->Taras: Because my favorite jym is closed forever)))
Taras->You: Oh man, Can I help you?
You->Taras: Maybe you will come in my house and drink some beer) and watching football?
Taras->You: Nice idea, I gonna come to you!
You->Taras: I will wait you.


####  VLAD FULL BUFFER CONTAINE  ####

SYSTEM->You: Hello dude, just write name of recipient in 'recipient' file!!!
Taras->You: Hello everyone!
Taras->You: How are you?
You->Taras: Hi, Taras.
You->Taras: I'm in depression!
Taras->You: Why, what is happened!
You->Taras: Because my favorite jym is closed forever)))
Taras->You: Oh man, Can I help you?
You->Taras: Maybe you will come in my house and drink some beer) and watching football?
Taras->You: Nice idea, I gonna come to you!
You->Taras: I will wait you.


##########   CLEAR BUFFER  ##########


## VLAD BUFFER STATUS AFTER CLEAR ###

=====   INFO BUFFER STATUS   =====

Size of buffer: 2048
Number of messages: 0
Amount of used space: 0

===== END INFO BUFFER STATUS =====

==== VLAD END SHOWS INFO ====
=================================================================
```
---

### For user `Oleg`

```
=================================================================
==== OLEG START SHOWS INFO ====
#### BUFFER SIZE FOR USER 'Oleg' ####

Size is: 1024

#### BUFFER STATUS BEFORE RESIZE ####

=====   INFO BUFFER STATUS   =====

Size of buffer: 1024
Number of messages: 6
Amount of used space: 303

===== END INFO BUFFER STATUS =====

#### RESIZE BUFFER TO 200  BYTES ####
echo: write error: Operation not permitted

#### BUFFER STATUS AFTER RESIZE  ####

=====   INFO BUFFER STATUS   =====

Size of buffer: 1024
Number of messages: 6
Amount of used space: 303

===== END INFO BUFFER STATUS =====


####    OLEG BUFFER CONTAINE     ####

Taras->You: Hello everyone!
Taras->You: How are you?
You->Taras: Hi, Taras.
You->Taras: I am so sad because are creator Vlad can't come up with a good dialogue
Taras->You: Ohh, it's bull sh*t, we wish good luck to the Vlad :)


####  OLEG FULL BUFFER CONTAINE  ####

SYSTEM->You: Hello dude, just write name of recipient in 'recipient' file!!!
Taras->You: Hello everyone!
Taras->You: How are you?
You->Taras: Hi, Taras.
You->Taras: I am so sad because are creator Vlad can't come up with a good dialogue
Taras->You: Ohh, it's bull sh*t, we wish good luck to the Vlad :)


##########   CLEAR BUFFER  ##########


## OLEG BUFFER STATUS AFTER CLEAR ###

=====   INFO BUFFER STATUS   =====

Size of buffer: 1024
Number of messages: 0
Amount of used space: 0

===== END INFO BUFFER STATUS =====

==== OLEG END SHOWS INFO ====
=================================================================


####################### END ##########################

```
### `dmesg`

```
[  201.753867] Messenger: ======== char device init ========
[  201.754498] Messenger: sysfs initialized
[  203.804415] Messenger: Buffer size is 1024
[  203.953775] Messenger: RESIZE buffer for Taras to 600 bytes!!!
[  204.311468] Messenger: Buffer size is 1024
[  204.444438] Messenger: RESIZE buffer for Vlad to 2048 bytes!!!
[  204.705458] Messenger: Buffer size is 1024
[  204.808556] Messenger: New buffer size is too small, minimum lenght is 256
[  205.102591] Messenger: sysfs files removed
[  205.103717] Messenger: =========== char devices remove ========
```
