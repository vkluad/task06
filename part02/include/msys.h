#ifndef __MSYS_H
#define __MSYS_H
#include <linux/sysfs.h>
#include <linux/cdev.h>
#include <linux/kern_levels.h>
#include <linux/slab.h>

#define INFO_OUT 256
#define LENGHT_OF_NUMBER 16

extern ssize_t user_store(struct class *class, struct class_attribute *attr,
			  const char *buf, size_t count);

extern ssize_t user_show(struct class *class, struct class_attribute *attr,
			 char *buf);

extern ssize_t get_buffer_status_show(struct class *class,
				      struct class_attribute *attr, char *buf);

extern ssize_t clear_buffer_store(struct class *class,
				  struct class_attribute *attr, const char *buf,
				  size_t count);

extern ssize_t buffer_size_show(struct class *class,
				struct class_attribute *attr, char *buf);

extern ssize_t buffer_size_store(struct class *class,
				 struct class_attribute *attr, const char *buf,
				 size_t count);

extern ssize_t recipient_show(struct class *class, struct class_attribute *attr,
			      char *buf);

extern ssize_t recipient_store(struct class *class,
			       struct class_attribute *attr, const char *buf,
			       size_t count);

extern int msys_init(void);
extern void msys_exit(void);

#endif //__MSYS_H
