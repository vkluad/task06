#ifndef __MESSENGER_H
#define __MESSENGER_H

#include <linux/module.h>

#define MAX_USER 16
#define MODNAME "Messenger"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Vlad Kudenchuk <vkluad@gmail.com>");
MODULE_DESCRIPTION("Simple messenger in kernel mode");

extern long dev_buffer_size;

extern void reverse(char s[]);
extern void itoa(long n, char s[]);

#endif // __MESSENGER_H
