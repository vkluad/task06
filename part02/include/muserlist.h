#ifndef __LIST_H
#define __LIST_H

#include "../include/messenger.h"
#include <linux/list.h>

#define MSG_SIZE 256

struct user_data {
	struct list_head list;
	unsigned int minor_id; // max is DEVICE_COUNT
	char *name, *ms_buf, *recipient_name;
	unsigned long buffer_lenght;
};

struct user_list {
	struct list_head headlist, *current_user;
	unsigned int lenght_list;
};

extern void user_list_init(void);

extern void user_list_exit(void);

extern int add_user_to_list(const char *name);

extern void change_cur_user_buf_size(const unsigned long new_size);

extern void clear_user_buffer(void);

extern int send_message(char *msg);

extern char *get_cur_user_buf(void);

extern char *get_cur_user_name(void);

extern char *get_cur_recipient_name(void);

extern char *read_message(void);

extern int change_recipient(const char *name);

extern int change_current_user(const char *name);

extern unsigned int get_cur_user_minor(void);

extern int get_cur_user_buf_size(void);

extern int get_cur_user_strlen_buf(void);

void free_user_data(struct user_data *data);

#endif
