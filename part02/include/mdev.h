#ifndef __DEV_H
#define __DEV_H

#include <linux/fs.h>
#include "../include/messenger.h"

#define DEVICE_FIRST 0
#define DEVICE_COUNT MAX_USER
#define NAME_LENGHT 16

extern int major;
extern void change_buf_size(const long new_size);
extern int change_user(const char *name);

extern struct class *mdev_class;
extern int dev_init(void);
extern void dev_exit(void);

extern ssize_t dev_read(struct file *file, char __user *buf, size_t lenght,
			loff_t *pos);
extern ssize_t dev_write(struct file *file, const char __user *buf,
			 size_t lenght, loff_t *pos);

#endif // __DEV_H
