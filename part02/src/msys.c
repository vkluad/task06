#include "../include/msys.h"
#include "../include/mdev.h"
#include "../include/messenger.h"
#include "../include/muserlist.h"
#include <linux/uaccess.h>

/* For set the user */

ssize_t user_show(struct class *class, struct class_attribute *attr, char *buf)
{
	strncpy(buf, get_cur_user_name(), strlen(get_cur_user_name()));

	printk(MODNAME ": user is %s\n", get_cur_user_name());
	return strlen(buf);
}

ssize_t user_store(struct class *class, struct class_attribute *attr,
		   const char *buf, size_t count)
{
	char *temp = kstrdup(buf, GFP_KERNEL), *name = temp;
	if (strcmp(strsep(&temp, ":\n"), "new") == 0) {
		name = strsep(&temp, ":\n");
		if (add_user_to_list(name) != 0) {
			count = -1;
			goto end;
		}
	}

	if (change_user(name) != 0) {
		count = -1;
		goto end;
	}
end:
	while (strsep(&temp, "\n") != NULL)
		;
	kfree(temp);
	return count;
}
/***************************************************/

/* For get buffer status */
ssize_t get_buffer_status_show(struct class *class,
			       struct class_attribute *attr, char *buf)
{
	char *temp_buf = kstrdup(get_cur_user_buf(), GFP_KERNEL),
	     info_output[INFO_OUT] = "", temp_size_s[LENGHT_OF_NUMBER] = "";
	int mess_count = 0;

	while (strsep(&temp_buf, "\n") != NULL)
		mess_count++;
	mess_count--;

	strncat(info_output, "=====   INFO BUFFER STATUS   =====\n\n", 36);
	strncat(info_output, "Size of buffer: ", 16);
	itoa(get_cur_user_buf_size(), temp_size_s);
	strncat(info_output, temp_size_s, strlen(temp_size_s));

	strncat(info_output, "\nNumber of messages: ", 21);
	strncpy(temp_size_s, "\000", strlen(temp_size_s));
	itoa(mess_count, temp_size_s);
	strncat(info_output, temp_size_s, strlen(temp_size_s));

	strncat(info_output, "\nAmount of used space: ", 23);
	strncpy(temp_size_s, "\000", strlen(temp_size_s));
	itoa(get_cur_user_strlen_buf(), temp_size_s);
	strncat(info_output, temp_size_s, strlen(temp_size_s));
	strncat(info_output, "\n\n===== END INFO BUFFER STATUS =====\n", 37);

	strncpy(buf, info_output, strlen(info_output));
	kfree(temp_buf);

	return strlen(buf);
}
/***************************************************/

/* For clear buffer */
ssize_t clear_buffer_store(struct class *class, struct class_attribute *attr,
			   const char *buf, size_t count)
{
	clear_user_buffer();
	return count;
}
/**************************************************/

/* For resize dev_buffer */
ssize_t buffer_size_show(struct class *class, struct class_attribute *attr,
			 char *buf)
{
	char temp[16];
	itoa(get_cur_user_buf_size(), temp);
	strncat(temp, "\n", 1);
	strncpy(buf, temp, strlen(temp));
	printk(KERN_NOTICE MODNAME ": Buffer size is %d",
	       get_cur_user_buf_size());
	return strlen(buf);
}

ssize_t buffer_size_store(struct class *class, struct class_attribute *attr,
			  const char *buf, size_t count)
{
	long new_buffer_size = 0;
	if (kstrtol(buf, 10, &new_buffer_size)) {
		printk(KERN_ERR MODNAME
		       ": only numbers enter, but you enter: %s",
		       buf);
		return -1;
	}
	if (MSG_SIZE > new_buffer_size) {
		printk(KERN_ERR MODNAME
		       ": New buffer size is too small, minimum lenght is %d",
		       MSG_SIZE);
		return -1;
	}

	change_buf_size(new_buffer_size);
	return count;
}
/**************************************************/

ssize_t recipient_show(struct class *class, struct class_attribute *attr,
		       char *buf)
{
	char *recipient_name = get_cur_recipient_name();
	strncpy(buf, recipient_name, strlen(recipient_name));

	return strlen(buf);
}

ssize_t recipient_store(struct class *class, struct class_attribute *attr,
			const char *buf, size_t count)
{
	char *sep_name = kstrdup(buf, GFP_KERNEL);
	sep_name = strsep(&sep_name, "\n");

	if (change_recipient(sep_name) != 0) {
		kfree(sep_name);
		return -1;
	}
	kfree(sep_name);
	return count;
}

CLASS_ATTR_RO(get_buffer_status);
CLASS_ATTR_RW(user);
CLASS_ATTR_RW(recipient);
CLASS_ATTR_WO(clear_buffer);
CLASS_ATTR_RW(buffer_size);

int msys_init(void)
{
	int res;

	if ((res = class_create_file(mdev_class,
				     &class_attr_get_buffer_status)))
		return res;
	if ((res = class_create_file(mdev_class, &class_attr_user)))
		return res;
	if ((res = class_create_file(mdev_class, &class_attr_clear_buffer)))
		return res;
	if ((res = class_create_file(mdev_class, &class_attr_buffer_size)))
		return res;
	if ((res = class_create_file(mdev_class, &class_attr_recipient)))
		return res;

	printk(KERN_NOTICE MODNAME ": sysfs initialized\n");
	return res;
}

void msys_exit(void)
{
	class_remove_file(mdev_class, &class_attr_get_buffer_status);
	class_remove_file(mdev_class, &class_attr_user);
	class_remove_file(mdev_class, &class_attr_clear_buffer);
	class_remove_file(mdev_class, &class_attr_buffer_size);
	class_remove_file(mdev_class, &class_attr_recipient);

	printk(KERN_NOTICE MODNAME ": sysfs files removed\n");
}
