#include "../include/muserlist.h"
#include <linux/slab.h>

#define get_user_data()                                                        \
	list_entry(local_user.current_user, struct user_data, list);

struct user_list local_user;

void user_list_init(void)
{
	INIT_LIST_HEAD(&(local_user.headlist));
	local_user.lenght_list = 0;
	add_user_to_list("user_manager");
	change_current_user("user_manager");
}

void user_list_exit(void)
{
	struct list_head *q;
	struct user_data *data;

	list_for_each_safe (local_user.current_user, q,
			    &(local_user.headlist)) {
		data = list_entry(local_user.current_user, struct user_data,
				  list);
		free_user_data(data);

		list_del(local_user.current_user);
		kfree(data);
	}
}

void free_user_data(struct user_data *data)
{
	kfree(data->name);
	kfree(data->recipient_name);
	kfree(data->ms_buf);
}

bool is_unique_name(const char *name)
{
	struct user_data *data;
	list_for_each_entry (data, &(local_user.headlist), list) {
		if (strcmp(data->name, name) == 0) {
			return 0;
		}
	}
	return 1;
}

int add_user_to_list(const char *name)
{
	struct user_data *data = (struct user_data *)kmalloc(
		sizeof(struct user_data), GFP_KERNEL);

	if (!is_unique_name(name))
		return -1;

	if (local_user.lenght_list >= MAX_USER)
		return -1;

	if (NULL != data) {
		data->name = kstrdup(name, GFP_KERNEL);
		data->recipient_name = kstrdup("everyone", GFP_KERNEL);
		data->ms_buf = kmalloc(dev_buffer_size, GFP_KERNEL);
		strncpy(data->ms_buf, "\000", dev_buffer_size);
		strncpy(data->ms_buf,
			"SYSTEM->You: Hello dude, just write name of recipient in 'recipient' file!!!\n",
			77);
		data->buffer_lenght = dev_buffer_size;
		data->minor_id = ++local_user.lenght_list;
		list_add_tail(&(data->list), &(local_user.headlist));
		return 0;
	}
	return -1;
}

int change_current_user(const char *name)
{
	struct user_data *data;
	struct list_head *q;
	list_for_each_safe (local_user.current_user, q,
			    &(local_user.headlist)) {
		data = list_entry(local_user.current_user, struct user_data,
				  list);
		if (strcmp(data->name, name) == 0) {
			return 0;
		}
	}

	printk(KERN_ERR MODNAME ": User name %s not found!!!\n", name);
	return -1;
}

char *get_cur_user_buf(void)
{
	struct user_data *data = get_user_data();
	return data->ms_buf;
}

unsigned int get_cur_user_minor(void)
{
	struct user_data *data = get_user_data();
	return data->minor_id;
}

int get_cur_user_strlen_buf(void)
{
	struct user_data *data = get_user_data();
	return strlen(data->ms_buf);
}

static void buffer_delete_old_msg(struct user_data *data,
				  const unsigned long new_msg_size)
{
	int del_lenght = 0;
	char *temp_ms_buf = NULL, *new_start_msg_buf = NULL;
	temp_ms_buf = kstrdup(data->ms_buf, GFP_KERNEL);

	if ((strlen(data->ms_buf) + new_msg_size) >= data->buffer_lenght) {
		while (del_lenght <= new_msg_size) {
			new_start_msg_buf = strsep(&temp_ms_buf, "\n");
			if (new_start_msg_buf != NULL)
				del_lenght += strlen(new_start_msg_buf);
		}

		strncpy(data->ms_buf, "\000", data->buffer_lenght);
		strncpy(data->ms_buf, temp_ms_buf, strlen(temp_ms_buf));
	}

	kfree(temp_ms_buf);
}

void change_cur_user_buf_size(const unsigned long new_size)
{
	struct user_data *data = NULL;
	char *temp_buf = NULL;
	data = get_user_data();
	data->buffer_lenght = new_size;

	if (strlen(data->ms_buf) > new_size)
		buffer_delete_old_msg(data,strlen(data->ms_buf) - new_size + 1);

	temp_buf = kstrdup(data->ms_buf, GFP_KERNEL);

	printk(KERN_NOTICE MODNAME
	       ": RESIZE buffer for %s to %lu bytes!!!\n",
	       data->name, new_size);

	if (data->ms_buf != NULL)
		kfree(data->ms_buf);

	data->ms_buf = kmalloc(new_size, GFP_KERNEL);
	strncpy(data->ms_buf, temp_buf, new_size);

	kfree(temp_buf);
}

char *get_cur_user_name(void)
{
	struct user_data *data = get_user_data();
	return data->name;
}

void clear_user_buffer(void)
{
	struct user_data *data = get_user_data();
	strncpy(get_cur_user_buf(), "\000", data->buffer_lenght);
}

int get_cur_user_buf_size(void)
{
	struct user_data *data = get_user_data();
	return data->buffer_lenght;
}

int change_recipient(const char *name)
{
	struct user_data *data = get_user_data();

	if (is_unique_name(name) && (strcmp(name, "everyone") != 0))
		return -1;

	kfree(data->recipient_name);
	data->recipient_name = kstrdup(name, GFP_KERNEL);

	return 0;
}

char *get_cur_recipient_name(void)
{
	struct user_data *data = get_user_data();
	return data->recipient_name;
}

int send_message(char *msg)
{
	struct user_data *recipient_data = NULL, *sender_data = NULL;
	char *msg_buff_for_sender = NULL, *msg_buff_for_recipient = NULL;
	msg = strsep(&msg, "\n");

	if (strlen(msg) >= MSG_SIZE)
		return -1;

	msg_buff_for_sender = kmalloc(MSG_SIZE, GFP_KERNEL);
	msg_buff_for_recipient = kmalloc(MSG_SIZE, GFP_KERNEL);

	strncpy(msg_buff_for_sender, "\000", MSG_SIZE);
	strncpy(msg_buff_for_recipient, "\000", MSG_SIZE);

	sender_data = get_user_data();

	/* for create msg_pack for sender */

	strncat(msg_buff_for_sender, "You->", 5);
	strncat(msg_buff_for_sender, sender_data->recipient_name,
		strlen(sender_data->recipient_name));
	strncat(msg_buff_for_sender, ": ", 2);
	strncat(msg_buff_for_sender, msg, strlen(msg));
	strncat(msg_buff_for_sender, "\n", 1);
	/* ****************************** */

	/* Create msg_pack for recipient */
	strncat(msg_buff_for_recipient, sender_data->name,
		strlen(sender_data->name));
	strncat(msg_buff_for_recipient, "->You", 5);
	strncat(msg_buff_for_recipient, ": ", 2);
	strncat(msg_buff_for_recipient, msg, strlen(msg));
	strncat(msg_buff_for_recipient, "\n", 1);
	/* ***************************** */

	buffer_delete_old_msg(sender_data, strlen(msg_buff_for_sender));

	strncat(sender_data->ms_buf, msg_buff_for_sender,
		strlen(msg_buff_for_sender));

	list_for_each_entry (recipient_data, &(local_user.headlist), list) {
		if (!strcmp(sender_data->recipient_name,
			    recipient_data->name)) {
			buffer_delete_old_msg(recipient_data,
					      strlen(msg_buff_for_recipient));
			strncat(recipient_data->ms_buf, msg_buff_for_recipient,
				strlen(msg_buff_for_recipient));
			break;
		}
		if ((!strcmp(sender_data->recipient_name, "everyone"))&&
			(recipient_data != sender_data)) {
			buffer_delete_old_msg(recipient_data,
					      strlen(msg_buff_for_recipient));
			strncat(recipient_data->ms_buf, msg_buff_for_recipient,
				strlen(msg_buff_for_recipient));
		}
	}

	kfree(msg_buff_for_sender);
	kfree(msg_buff_for_recipient);
	return 0;
}

char *read_message(void)
{
	struct user_data *data = NULL;
	char *read_buf = NULL, *temp_buf = NULL, *temp_sep_buf,
	     *temp_buf_copy = NULL;

	data = get_user_data();
	read_buf = kmalloc(get_cur_user_buf_size(), GFP_KERNEL);
	temp_buf = kstrdup(get_cur_user_buf(), GFP_KERNEL);
	temp_buf_copy = kstrdup(temp_buf, GFP_KERNEL);

	strncpy(read_buf, "\000", get_cur_user_buf_size());

	if (strcmp(data->recipient_name, "everyone") == 0) {
		kfree(temp_buf);
		kfree(temp_buf_copy);
		kfree(read_buf);
		read_buf = kstrdup(get_cur_user_buf(), GFP_KERNEL);
		return read_buf;
	}

	while ((temp_sep_buf = strsep(&temp_buf, "\n")) != NULL) {
		char *name, *recipient_name, *string;

		name = strsep(&temp_sep_buf, "-");

		if ((name == NULL) || (strlen(name) == 0))
			break;

		recipient_name = strsep(&temp_sep_buf, ":");
		if ((recipient_name == NULL) || (strlen(recipient_name) == 0))
			break;
		recipient_name++;

		string = strsep(&temp_sep_buf, "\n");
		if ((string == NULL) || (strlen(string) == 0))
			break;

		if ((strcmp(name, data->recipient_name) == 0) ||
		    (strcmp(recipient_name, data->recipient_name) == 0) ||
		    (strcmp(recipient_name, "everyone") == 0)) {
			char *tmp_str = strsep(&temp_buf_copy, "\n");
			strncat(read_buf, tmp_str, strlen(tmp_str));
			strncat(read_buf, "\n", 1);
			continue;
		}
		strsep(&temp_buf_copy, "\n");
	}

	kfree(temp_buf);

	while (strsep(&temp_buf_copy, "\n") != NULL)
		kfree(temp_buf_copy);

	return read_buf;
}
