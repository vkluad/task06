#include "../include/mdev.h"
#include "../include/muserlist.h"
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/kern_levels.h>
#include <linux/slab.h>

static struct cdev hcdev;
struct class *mdev_class = NULL;

void change_buf_size(const long new_size)
{
	change_cur_user_buf_size(new_size);
	dev_buffer_size = new_size;
}

int change_user(const char *name)
{
	dev_t dev;

	dev = MKDEV(major, get_cur_user_minor());
	device_destroy(mdev_class, dev);
	if (change_current_user(name) != 0)
		return 1;

	if (get_cur_user_minor() <= DEVICE_COUNT) {
		dev = MKDEV(major, get_cur_user_minor());
		device_create(mdev_class, NULL, dev, NULL, "%s", name);
	}
	return 0;
}

static const struct file_operations dev_fops = { .owner = THIS_MODULE,
						 .read = dev_read,
						 .write = dev_write };

ssize_t dev_read(struct file *file, char __user *buf, size_t lenght,
		 loff_t *pos)
{
	char *buffer_to_user = read_message();
	int len = strlen(buffer_to_user);

	if (lenght < len)
		return -EINVAL;

	if (*pos != 0) {
		*pos = 0;
		return 0;
	}

	if (copy_to_user(buf, buffer_to_user, len))
		return -EINVAL;

	*pos = len;
	kfree(buffer_to_user);

	return len;
}

ssize_t dev_write(struct file *file, const char __user *buf, size_t lenght,
		  loff_t *pos)
{
	char *temp_buf = kmalloc(lenght, GFP_KERNEL);

	if (copy_from_user(temp_buf, buf, lenght)) {
		lenght = -EINVAL;
		goto end;
	}

	if (send_message(temp_buf) != 0)
		printk(KERN_ERR MODNAME
		       ": Your MESSAGE is too long, max size is %d !!!\n",
		       MSG_SIZE);

	*pos = 0;
end:
	kfree(temp_buf);

	return lenght;
}

int dev_init(void)
{
	int ret;
	dev_t dev;

	user_list_init();

	if (major != 0) {
		dev = MKDEV(major, DEVICE_FIRST);
		ret = register_chrdev_region(dev, DEVICE_COUNT, MODNAME);
	} else {
		ret = alloc_chrdev_region(&dev, DEVICE_FIRST, DEVICE_COUNT,
					  MODNAME);
		major = MAJOR(dev);
	}
	if (ret < 0) {
		printk(KERN_ERR MODNAME
		       ": === Can not register char device region\n");
		goto err;
	}

	cdev_init(&hcdev, &dev_fops);
	hcdev.owner = THIS_MODULE;
	ret = cdev_add(&hcdev, dev, DEVICE_COUNT);

	if (ret < 0) {
		unregister_chrdev_region(MKDEV(major, DEVICE_FIRST),
					 DEVICE_COUNT);
		printk(KERN_ERR MODNAME ": === Can not add char device\n");
		goto err;
	}
	mdev_class = class_create(THIS_MODULE, "Messenger");
	if (IS_ERR(mdev_class)) {
		printk(KERN_ERR MODNAME ": bad class sysfs create\n");
		return -1;
	}

	if (change_user(get_cur_user_name()) != 0)
		printk(KERN_ERR MODNAME ": dev_init user not found!!!\n");

	printk(KERN_NOTICE MODNAME ": ======== char device init ========\n");
err:
	return ret;
}

void dev_exit(void)
{
	dev_t dev;
	//	int i;
	/*
          for( i = 0; i < DEVICE_COUNT; i++ ) {
                  dev = MKDEV( major, DEVICE_FIRST + i );
                  device_destroy( devclass, dev );
          }
  */
	dev = MKDEV(major, get_cur_user_minor());
	device_destroy(mdev_class, dev);

	class_destroy(mdev_class);
	cdev_del(&hcdev);
	unregister_chrdev_region(MKDEV(major, DEVICE_FIRST), DEVICE_COUNT);

	user_list_exit();

	printk(KERN_INFO MODNAME
	       ": =========== char devices remove ========\n");
}
