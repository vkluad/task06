#include "../include/messenger.h"
#include "../include/mdev.h"
#include "../include/msys.h"
#include "../include/muserlist.h"
#include <linux/kernel.h>
#include <linux/slab.h>

long dev_buffer_size = 1024;

int major = 511;

module_param(dev_buffer_size, long, S_IRUGO);

module_param(major, int, S_IRUGO);

static int __init mes_init(void)
{
	int ret;
	ret = dev_init();
	if (ret != 0)
		return ret;

	ret = msys_init();
	if (ret != 0)
		return ret;

	return 0;
}

static void __exit mes_exit(void)
{
	msys_exit();
	dev_exit();
}

module_init(mes_init);
module_exit(mes_exit);
