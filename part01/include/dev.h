#ifndef __DEV_H
#define __DEV_H

#include <linux/fs.h>

#define DEVICE_FIRST  0
#define DEVICE_COUNT  16
#define MODNAME "messenger"

extern long buffer_size;
extern char *dev_buffer;

extern int dev_init(void);
extern void dev_exit(void);

extern ssize_t dev_read(struct file* file, char __user *buf, size_t lenght, loff_t *pos);
extern ssize_t dev_write(struct file* file, const char __user *buf, size_t lenght, loff_t *pos);



#endif // __DEV_H
