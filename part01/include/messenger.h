#ifndef __MESSENGER_H
#define __MESSENGER_H

#include <linux/module.h>

#define MODULE_TAG "Messenger"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Vlad Kudenchuk <vkluad@gmail.com>");
MODULE_DESCRIPTION("Simple messenger in kernel mode");


#endif // __MESSENGER_H
