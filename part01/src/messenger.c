#include <linux/slab.h>
#include <linux/kernel.h>
#include "../include/messenger.h"
#include "../include/dev.h"


long buffer_size = 1024;

module_param(buffer_size, long, S_IRUGO );

char *dev_buffer = NULL;
	
static int __init mes_init(void){
	int ret;
	ret = dev_init();
	
	if (ret != 0 ) return ret;
	
	dev_buffer = kmalloc(buffer_size, GFP_KERNEL);
	strncpy(dev_buffer,"Hello everyone!!!\n", 18);
	return 0;
}

static void __exit mes_exit(void){
	kfree(dev_buffer);
	dev_exit();
}

module_init(mes_init);
module_exit(mes_exit);
