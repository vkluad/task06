#include "../include/dev.h"
#include "../include/messenger.h"
#include <linux/slab.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/kern_levels.h>


static int major = 511;
// static int minor_now = 0;

static struct cdev hcdev;
static struct class *devclass;



/*
static char *user[DEVICE_COUNT];

static void change_user(char * name){
	dev_t dev;
	dev = MKDEV( major, minor_now );
	device_destroy( devclass, dev );
	
	for( i = 0; i < DEVICE_COUNT; i++ ) {
		if(strcmp(name, user[i]) == 0){
			dev = MKDEV( major, DEVICE_FIRST + i );
			device_create( devclass, NULL, dev, NULL, "%s", name );
			minor_now = DEVICE_FIRST + i;
			break;
		}
	}

}
*/

static const struct file_operations dev_fops = {
	.owner = THIS_MODULE,
	.read  = dev_read,
	.write = dev_write
	};


ssize_t dev_read(struct file* file, char __user *buf, size_t lenght, loff_t *pos){
	int len = strlen( dev_buffer );
	
	printk( KERN_INFO MODULE_TAG ": === read dev : %ld\n", (long)lenght );
	
	if( lenght < len ) return -EINVAL;
	
	if( *pos != 0 ) {
		*pos = 0;
		return 0;
	}
	
	if( copy_to_user( buf, dev_buffer , len ) ) return -EINVAL;

	*pos = len;

	// printk( KERN_INFO "=== read return : %d\n", len );
	return len;

}

ssize_t dev_write(struct file* file, const char __user *buf, size_t lenght, loff_t *pos){

	char *temp_buf = kmalloc(lenght, GFP_KERNEL);

	printk(KERN_INFO MODULE_TAG ": === write dev : %ld\n", lenght);
	
	if( copy_from_user( temp_buf, buf, lenght )) return -EINVAL;
	
	if( (strlen(dev_buffer) + lenght) >= buffer_size ){
		char  *temp_dev_buffer = kstrdup(dev_buffer, GFP_KERNEL), *temp_dev_buffer_start = temp_dev_buffer, *new_start_temp_buf = NULL;
		int del_lenght = 0;		
		
		while (del_lenght >= lenght){
			new_start_temp_buf = strsep(&temp_dev_buffer,"\n");
			del_lenght += strlen(new_start_temp_buf);
			}
		strncpy(temp_buf, "\000", buffer_size);
		strncpy(dev_buffer, temp_dev_buffer, strlen(temp_dev_buffer));
		kfree(temp_dev_buffer_start);
		}
	strncat(dev_buffer, temp_buf, lenght);
	kfree(temp_buf);
	*pos = 0;
	
	return lenght;	
}

int dev_init(void){
	int ret;
	dev_t dev;
	
	if( major != 0 ) {
		dev = MKDEV( major, DEVICE_FIRST );
		ret = register_chrdev_region( dev, DEVICE_COUNT, MODNAME );
	}
	else {
		ret = alloc_chrdev_region( &dev, DEVICE_FIRST, DEVICE_COUNT, MODNAME );
		major = MAJOR( dev );
	}
	if( ret < 0 ) {
		printk( KERN_ERR MODULE_TAG ": === Can not register char device region\n" );
		goto err;
	}
	
	cdev_init( &hcdev, &dev_fops );
	hcdev.owner = THIS_MODULE;
	ret = cdev_add( &hcdev, dev, DEVICE_COUNT );

	if( ret < 0 ) {
		unregister_chrdev_region( MKDEV( major, DEVICE_FIRST ), DEVICE_COUNT );
		printk( KERN_ERR MODULE_TAG ": === Can not add char device\n" );
		goto err;
	}
	devclass = class_create( THIS_MODULE, "messenger" );
	dev = MKDEV( major, DEVICE_FIRST );

	device_create( devclass, NULL, dev, NULL, "user_messenger" );
err:
	return ret;
}

void dev_exit(void){
	dev_t dev;
//	int i;
/*
	for( i = 0; i < DEVICE_COUNT; i++ ) {
		dev = MKDEV( major, DEVICE_FIRST + i );
		device_destroy( devclass, dev );
	}
*/
	dev = MKDEV( major, DEVICE_FIRST );
	device_destroy( devclass, dev );
	
	class_destroy( devclass );
	cdev_del( &hcdev );
	unregister_chrdev_region( MKDEV( major, DEVICE_FIRST ), DEVICE_COUNT );
	printk( KERN_INFO MODULE_TAG ": =========== char devices remove ========\n" );
}







